Template.register.helpers({

});
Template.register.events({
	"click #register": function(){
		var email= $('[name=email]').val();
		var password= $('[name=password]').val();
        var object= new User.Model();
        object.email=email;
        object.profile.email=email;
        object.password=password;

		if(isEmail(email)&&isValidPassword(password)){
			Accounts.createUser(object);
		}
		
		Router.go("/userhome");
	}
});
isEmail = function(value) {
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (filter.test(value)) {
        return true;
    }
    console.log('Please enter a valid email address.');
    return false;
};
isNotEmpty = function(value) {
    if (value && value !== ''){
        return true;
    }
    console.log('Please fill in all required fields.');
    return false;
};
isValidPassword = function(password) {
    if (password.length < 6) {
        console.log('Your password should be 6 characters or longer.');
        return false;
    }
    return true;
};