Template.userhome.helpers({
	images: function () {
		var createdBy = Meteor.userId();
	    return Images.find({createdBy:createdBy}); 
	}
});
Template.userhome.events({
	"click #add": function(){
		var url=$("#url").val();
		var title=$("#title").val();
		var price=$("#price").val();
		var note=$("#note").val();
		var region=$("#region").val();
		var country=$("#country").val();
		var style=$("#style").val();
		var createdBy=Meteor.userId();
		
		var object = new Images.Model();
			object.url = url;
			object.title = title;
			object.price = price;
			object.note = note;
			object.region = region;
			object.country = country;
			object.style = style;
			object.createdBy = createdBy;		
			object.createdAt= new Date();

		
		if(isNotEmpty(url) && isNotEmpty(title) && isNotEmpty(price))
		{
				Images.insert(object);
		}
		else{
			window.alert("enter all fields");
		}
	
	
		$("#url").val(" ");
		$("#title").val(" ");
		$("#price").val(" ");
		$("#note").val(" ");
		$("#region").val(" ");
		$("#country").val(" ");
		$("#style").val(" ");
	},
	"click #delete": function(){
		Images.remove(this._id);
	}
});
isNotEmpty = function(value) {
    if (value && value !== ''){
        return true;
    }
    console.log('Please fill in all required fields.');
    return false;
};
