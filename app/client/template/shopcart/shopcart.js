
Template.shopcart.helpers({
  items: function(){
    if(!Meteor.user())
      return;
    return Meteor.user().profile.cart;

  },

  total: function(){
    var total = 0;
    if(!Meteor.user())
      return;
    var list = Meteor.user().profile.cart;
    for (var i = 0; i < list.length; i++) {

      var sum=list[i].subTotal;
      total = total + parseInt(sum);
    };
    return total;

  },
  cartno: function(){
     var list = Meteor.user().profile.cart;
    var crtno=list.length;
    return crtno;
  }
// carts:function() 
//   {
//     var scart=Meteor.user().profile.cart;
//     var spkart=[];
//     for(i=0;i<=3;i++)
//     {
//       var topPosts= Images.find({_id:scart[i]});
//       topPosts.forEach(function (post) {
//         var titles= post;
//         spkart.push(titles);
//         console.log(titles);
//         });
//     }
//     return spkart;
//   }
});

Template.shopcart.events({
   "click #delete": function(){
    var qty= $("#qty").val();
    var item_id=this.item_id;
    Meteor.call("removeCartItem",item_id,Meteor.userId());

  },
  "click #clear": function(){
    
    Meteor.call("clearCartItem",Meteor.userId());

  },
  "change #qty": function(e){
    var qtyno=$(e.target).val();
    var item_id=this.item_id;
    console.log(qtyno);
    console.log(item_id);
    Meteor.call("updateCartItem",item_id,qtyno,Meteor.userId());
  }
});