Template.navbar.events({
	"click #logout":function(event){
		event.preventDefault();
		Meteor.logout();	
		Router.go("/");

	}
});
Template.navbar.helpers({
  cartno: function(){
     var list = Meteor.user().profile.cart;
    var crtno=list.length;
    return crtno;
  }
});