Images= new Meteor.Collection("images");

Images.Model = function(){
	var model = {};
	model.url = "";
	model.title = "";
	model.price = 0;
	model.note = "";
	model.region = "";
	model.country = "";
	model.style = "";
	model.createdBy = "";		
	model.createdAt = new Date();
	return model;
}