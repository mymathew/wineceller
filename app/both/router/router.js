 Router.configure({
 	layoutTemplate: "main"
 });


// Router.route("/",{
// 	template: "home"
// });
// Router.route("/register",{
// 	template:"register"
// });
// Router.route("/login",{
// 	template:"login"
// });
// Router.route("/userhome/",{
// 	template:"userhome",
//     onBeforeAction: function(){
// 		var currentUser = Meteor.userId();
// 		if(currentUser){
// 			this.next();
// 		}
// 		else{
// 			this.render("login");
// 		}
// 	}
// });
// Router.route("/featuredwine",{
// 	template:"featuredwine"
// });
Router.map(function () {
	this.route("home",{
		path: '/',
		template: "home"
	});
	this.route("register",{
		path: '/register',
		template:'register'
	});
	this.route("login",{
		path: '/login',
		template: "login"
	});
	this.route("userhome",{
		path: '/userhome',
		template: "userhome",
		onBeforeAction:function(){
			var currentUser =Meteor.userId();
			if(currentUser){
		this.next();
		}
		else{
		this.render("login");
		}
 		}
	});
	this.route("featuredwine",{
		path: '/featuredwine',
		template: "featuredwine"
	});
	this.route("wine",{
		path: '/wine/:page_no',
		template: "featuredwine"
	});
	this.route("wineinfo",{
		path: "/wineinfo/:image_id",
		template: "wineinfo"
	});
	this.route("shopcart",{
		path: "/shopcart",
		template: "shopcart"
	});
})